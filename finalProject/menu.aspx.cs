﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalProject
{
    public partial class menu : System.Web.UI.Page
    {

        private string query = "select * from mypages";

        protected void Page_Load(object sender, EventArgs e)
        {
            menupage.SelectCommand = query;
            DataView view = (DataView)menupage.Select(DataSourceSelectArguments.Empty);

            string menu_text = "";
            foreach(DataRowView row in view)
            {
                menu_text += "<li><a href=\"page.aspx? pageid = "
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "<a></li>";
            }
             nav_menu.InnerHtml = menu_text;
        }
    }
}