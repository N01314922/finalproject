﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalProject
{
    public partial class edit_table : System.Web.UI.Page
    {

        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                edit_class.InnerHtml = "No Page Found.";
                return;
            }
            pageTitle.Text = pagerow["pagetitle"].ToString();

            pageContent.Text = pagerow["pagecontent"].ToString();

        }

        protected void edit_click(object sender, EventArgs e)
        {
           
            string title = pageTitle.Text;
            string content = pageContent.Text;
           
             string editquery = "Update mypages set pagetitle='" + title + "'," +
                " pagecontent='" + content + "'" +
                "where pageid=" + pageid;
            editTable.UpdateCommand = editquery;
            editTable.Update();
        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from mypages where pageid=" + pageid.ToString();
            editTable.SelectCommand = query;
          
            DataView pageview = (DataView)editTable.Select(DataSourceSelectArguments.Empty);

          
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; 
            return pagerowview;

        }


    }
}

