﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddPage.aspx.cs" Inherits="finalProject.AddPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


        <h3 class="edit_class" runat="server">Add Page</h3>

    
    <asp:SqlDataSource runat="server" id="addpage"
       ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>

    <div class="row">
        <label> page Title: </label>
        <asp:TextBox runat="server" ID="pageTitle"></asp:TextBox>
    </div>
    <div class="row">
        <label> page Content: </label>
        <asp:TextBox runat="server" ID="pageContent"></asp:TextBox>
    </div>

    <asp:Button runat="server" Text="add page" OnClick="add_click"/>






</asp:Content>
