﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalProject
{
    public partial class W3School : System.Web.UI.Page
    {

        private string basequery = "SELECT pageid,pagetitle,pagecontent from mypages"; 



        protected void Page_Load(object sender, EventArgs e)
        {

            classes_select.SelectCommand = basequery;
            classes_list.DataSource = Classes_Manual_Bind(classes_select);
            classes_list.DataBind();


        }

        protected DataView Classes_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["pagetitle"] =
                    "<a href=\"page2.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

            }
         
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;

        }

        protected void addbutton(object sender, EventArgs e)
        {
            Response.Redirect("AddPage.aspx");
        }
    }
}