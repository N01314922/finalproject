﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="edit_table.aspx.cs" Inherits="finalProject.edit_table" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 id="edit_class" runat="server">Edit Table</h3>

    
    <asp:SqlDataSource runat="server" id="editTable"
       ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>

    <div class="row">
        <label> page Title: </label>
        <asp:TextBox runat="server" ID="pageTitle"></asp:TextBox>
    </div>
    <div class="row">
        <label> page Content </label>
        <asp:TextBox runat="server" ID="pageContent" TextMode="MultiLine" Columns="30" Rows="5"></asp:TextBox>
    </div>

    <asp:Button runat="server" Text="edit page" OnClick="edit_click"/>

</asp:Content>
