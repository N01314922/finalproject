﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalProject
{
    public partial class page2 : System.Web.UI.Page
    {

        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        private string basequery = "SELECT pageid,pagetitle,pagecontent from mypages";


        protected void Page_Load(object sender, EventArgs e)
        {

            if (pageid == "" || pageid == null) page_name.InnerHtml = "No class found.";

            else
            {
                basequery += " WHERE PAGEID = " + pageid;

                page_class.SelectCommand = basequery;


                DataView classview = (DataView)page_class.Select(DataSourceSelectArguments.Empty);
                string name = classview[0]["pagetitle"].ToString();
                page_name.InnerHtml = name;
                string fetch_content = classview[0]["pagecontent"].ToString();
                fetch.InnerHtml = fetch_content;



            }


        }

        protected void editpage(object sender, EventArgs e)
        {
            Response.Redirect("edit_table.aspx");

        }

        protected void DelClass(object sender, EventArgs e)
        {

            string delquery = "DELETE FROM mypages WHERE pageid=" + pageid;


            del_class.DeleteCommand = delquery;
            del_class.Delete();

        }

    }
}