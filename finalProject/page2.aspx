﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="page2.aspx.cs" Inherits="finalProject.page2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

   <h3 id="page_name" runat="server"> </h3>

     
     <asp:SqlDataSource  runat="server"
        id="page_class"
        ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>

    <div id="fetch" runat="server"></div>
    <a href="edit_table.aspx?pageid=<%Response.Write(this.pageid); %>">Edit Page</a>
  


    <asp:SqlDataSource  runat="server" id="del_class"    ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>
     <asp:Button runat="server" id="del_class_btn"
        OnClick="DelClass"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
   

</asp:Content>
